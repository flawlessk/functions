/*
ორი გუნდი ეჯიბრება ერთმანეთს კერლინგში, გუნდი PowerRangers და გუნდი FairyTails. თითოეული გუნდი ეჯიბრება ერთმანეთს 3-3 ჯერ, 
შემდეგ კი ითვლება ამ ქულების საშუალო (თითოეულ გუნდს თავისი საშუალო აქვს), გუნდი იგებს მხოლოდ მაშინ, თუ კი მისი საშუალო 
ორჯერ მაინც იქნება მეტი მეორე გუნდის საშუალოზე, წინააღმდეგ შემთხვევაში, გამარჯვებული არავინ არ არის. თქვენი დავალებაა:
1.1. შექმენათ ფუქნცია (calcAverage), რომელიც დაითვლის 3 ქულის საშუალო არითმეტიკულს
1.2. გამოიყენათ ფუნქცია თითოეული გუნდის საშუალოს დასათვლელად
1.3. შექმენათ ფუნქცია რომელიც შეამოწმებს გამარჯვებულ გუნდს (ex. checkWinner),  მიიღებს ორ პარამეტრს გუნდების საშუალოს 
სახით და კონსოლში დალოგავს გამარჯვებულ გუნდს თავისი ქულებით (მაგ. პირველმა გუნდმა გაიმარჯვა 31 vs 15 ქულა).
1.4 გამოყენეთ ეს ფუნქცია რომ გამოავლინოთ გამარჯვებული გუნდი 1. PowerRangers (44, 23, 71) , FairyTails (65, 54, 49) 2. PowerRangers (85, 54, 41) , FairyTails (23, 34, 47)
1.5 optional : ფუნქციაში შეგიძლიათ გაითვალისწინოთ ყაიმის შემთხვევაც.
*/

let teams = [
    {
        name: "PowerRangers",
        results: [
            [44, 23, 71],
            [85, 54, 41]
        ],
        averages: []
    },
    {
        name: "FairyTails",
        results: [
            [65, 54, 49],
            [23, 34, 47]
        ],
        averages: []
    }
];

function calcAverage(results) {
    let sum = 0;
    for (let i = 0; i < results.length; i++) {
        sum += results[i];
    }
    return sum / results.length;
}

for (let i = 0; i < teams.length; i++) {
    for (let j = 0; j < teams[i].results.length; j++) {
        teams[i].averages.push(calcAverage(teams[i].results[j]));
    }
}

function checkWinner(firstAverage, secondAverage) {
    if (firstAverage > 2 * secondAverage) {
        console.log(`პირველმა გუნდმა გაიმარჯვა ${firstAverage} vs ${secondAverage} ქულა`);
    } else if (firstAverage * 2 < secondAverage) {
        console.log(`მეორე გუნდმა გაიმარჯვა ${secondAverage} vs ${firstAverage} ქულა`);
    } else {
        console.log(`ფრე`);
    }
}

for (let i = 0; i < teams[0].averages.length; i++) {   // 0 იმიტო ავიღე რომ რამდენიც პირველ გუნდს ექნება იმდენი ექნება მეორესაც 
    checkWinner(teams[0].averages[i], teams[1].averages[i]);
}

console.log(teams);
